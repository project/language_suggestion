<?php

declare(strict_types=1);

namespace Drupal\Tests\language_suggestion\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Test description.
 *
 * @group language_suggestion
 */
final class LanguageSuggestionTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['language_suggestion'];

  /**
   * Test callback.
   */
  public function testSetup(): void {
    $admin_user = $this->drupalCreateUser(['access administration pages', 'administer site configuration']);
    $this->drupalLogin($admin_user);
    $this->drupalGet(Url::fromRoute('language_suggestion.settings'));
    $this->assertSession()->pageTextContains('Show language suggestion.');
  }

}
