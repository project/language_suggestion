(function ($, Drupal, drupalSettings, cookies) {
  $(document).ready(function () {
    const current_lang =
      drupalSettings.language_suggestion.current_language.toLowerCase();
    const settings = drupalSettings.language_suggestion.settings;

    if (settings.enabled) {
      // This code disables auto redirect when a visitor decides to switch languages in the UI.
      if (settings.disable_redirect_class) {
        $(document).on('click', settings.disable_redirect_class, function (e) {
          cookies.remove(`${settings.cookie_prefix}.always_redirect`);
          cookies.remove(`${settings.cookie_prefix}.redirect_lang`);
        });
      }

      if (settings.language_detection == 'browser') {
        // Browser-based redirection
        var browser_lang =
          window.navigator.userLanguage || window.navigator.language; // Set to 'de' for debugging.
        languageSuggestionRoutine();
      } else if (
        settings.language_detection == 'geoip_db' ||
        settings.language_detection == '_server'
      ) {
        // Check geoip.
        var browser_lang = '';
        var xhr = $.ajax({
          type: 'GET',
          url: `/language-suggestion/get-county-code?rk=${Math.random()
            .toString(36)
            .slice(-5)}`,
          success(output, status) {
            if (typeof output === 'object' && output !== null) {
              if (output.hasOwnProperty('country')) {
                if (output.country != '') {
                  browser_lang = output.country.toLowerCase();
                  languageSuggestionRoutine();
                }
              }
            }
          },
          error(output) {},
        });
      } else {
        // HTTP header-based redirection
        var browser_lang = '';
        var xhr = $.ajax({
          type: 'GET',
          url: '/language-suggestion/get-header-parameter',
          success(output, status) {
            browser_lang = output;
            languageSuggestionRoutine();
          },
          error(output) {},
        });
      }
    }

    function languageSuggestionRoutine() {
      const layoutContainer = $(
        settings.container_class !== undefined ||
          settings.container_class !== ''
          ? settings.container_class
          : 'body',
      );
      let show_suggestion = false;
      let lang_code = null;
      let continue_link = null;
      let message = null;
      let url = null;
      let custom_url = null;
      let default_url = null;

      const dismissed = cookies.get(`${settings.cookie_prefix}.dismiss`);
      const redirectLang = cookies.get(
        `${settings.cookie_prefix}.always_redirect`,
      );
      const redirectLangCode = cookies.get(
        `${settings.cookie_prefix}.redirect_lang`,
      );

      const date = new Date();
      const timestamp = date.getTime();

      // Auto redirect to previously selected language.
      // Also making sure we are not creating a redirect loop when already switch to a language.
      if (
        redirectLang !== undefined &&
        settings.always_redirect &&
        redirectLangCode !== undefined &&
        redirectLangCode !== current_lang
      ) {
        window.location.href = redirectLang;
      }

      // Looping through available language mapping to find if any mapped languages matching to a visitors browser language.
      for (const langcode in settings.mapping) {
        if (settings.mapping.hasOwnProperty(langcode)) {
          const langObject = settings.mapping[langcode];
          const codeArr =
            langObject.browser_lang.indexOf(',') > -1
              ? langObject.browser_lang.split(',')
              : [langObject.browser_lang];
          let index = 0;

          while (index < codeArr.length) {
            if (
              langcode != current_lang &&
              codeArr[index] == browser_lang.toLowerCase()
            ) {
              show_suggestion = true;
              lang_code = langcode;
              continue_link = langObject.continue_link;
              message = langObject.message;
              url = langObject.url;
              custom_url = langObject.custom_url;
              default_url = langObject.default_url;
            }
            index++;
          }
        }
      }

      // Show the language suggestion box. Make sure user hasn't dismissed in the past or dismiss hasn't expired yet.
      if (
        show_suggestion &&
        message &&
        (dismissed <= timestamp || dismissed === undefined)
      ) {
        layoutContainer.append(
          `<div id="language-suggestion" class="ls-wrapper"><div class="ls-message">${message}</div><div class="ls-goto"><a href="#" id="ls-continue">${
            continue_link !== undefined ? continue_link : Drupal.t('Continue')
          }</a></div><div class="ls-dismiss"><a href="#" id="ls-dismiss">${Drupal.t(
            'Dismiss',
          )}</a></div></div></div>`,
        );
        layoutContainer
          .find('#language-suggestion')
          .delay(settings.show_delay * 1000)
          .show('slow');
      }

      // Continue to the language suggested and make sure we add to auto redirect cookie if such option is enabled in the module settings.
      layoutContainer.find('#ls-continue').on('click', function (e) {
        const redirect = getRedirectUrl(url, custom_url, default_url);
        if (settings.always_redirect) {
          cookies.set(`${settings.cookie_prefix}.always_redirect`, redirect);
          cookies.set(`${settings.cookie_prefix}.redirect_lang`, lang_code);
        }
        window.location.href = redirect;
        e.disableDefault();
      });

      // Dismiss language suggestion box and make sure we keep it dismissed for some time. Time can be configured in the module settings.
      layoutContainer.find('#ls-dismiss').on('click', function (e) {
        layoutContainer.find('#language-suggestion').hide('slow');
        const milliseconds = settings.cookie_dismiss_time * 60 * 60 * 1000;
        cookies.set(
          `${settings.cookie_prefix}.dismiss`,
          timestamp + milliseconds,
        );
        e.disableDefault();
      });
    }

    function getRedirectUrl(type, custom_url, default_url) {
      switch (type) {
        case 'custom':
          return custom_url;
          break;
        default:
          return default_url;
          break;
      }
    }
  });
})(jQuery, Drupal, drupalSettings, window.Cookies);
