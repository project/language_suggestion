<?php

declare(strict_types=1);

namespace Drupal\language_suggestion\Plugin\LanguageNegotiation;

use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\language\LanguageNegotiationMethodBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class for identifying language HTTP header.
 *
 * @LanguageNegotiation(
 *   id = \Drupal\language_suggestion\Plugin\LanguageNegotiation\LanguageNegotiationHTTPHeader::METHOD_ID,
 *   weight = -3,
 *   name = @Translation("HTTP Header"),
 *   description = @Translation("Language from the HTTP header."),
 *   config_route_name = "language_suggestion.negotiation_http_header"
 * )
 */
class LanguageNegotiationHTTPHeader extends LanguageNegotiationMethodBase implements ContainerFactoryPluginInterface {

  /**
   * The language negotiation method id.
   */
  public const METHOD_ID = 'language-http-header';

  public function __construct(
    protected KillSwitch $killSwitch,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $container->get('page_cache_kill_switch'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getLangcode(?Request $request = NULL) {
    $langCode = FALSE;
    $config = $this->config->get('language_suggestion.language_negotiation');
    $header_param = $config->get('header_param');

    if ($this->languageManager && $request && $header_param && $request->headers->has($header_param)) {
      $http_header_lang = strtolower($request->headers->get($header_param));
      $mapping = $config->get('mapping');
      foreach ($mapping as $item) {
        if ($codes = explode(',', strtolower($item['http_language']))) {
          foreach ($codes as $code) {
            if ($http_header_lang === $code) {
              $langCode = $item['language'];
              break 2;
            }
          }
        }
      }
    }

    $this->killSwitch->trigger();

    return $langCode;
  }

}
