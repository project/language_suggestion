<?php

declare(strict_types=1);

namespace Drupal\language_suggestion\Controller;

use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\file\Entity\File;
use GeoIp2\Database\Reader;
use GeoIp2\Exception\AddressNotFoundException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Language Suggestion controller.
 */
class LanguageSuggestionController extends ControllerBase {

  public function __construct(
    ConfigFactoryInterface $config_factory,
    protected LoggerInterface $logger,
  ) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('logger.channel.language_suggestion'),
    );
  }

  /**
   * Get HTTP parameter value.
   */
  public function getHttpHeaderValue(Request $request) {
    $config = $this->configFactory->get('language_suggestion.settings');
    $langCode = '';
    $data = [];
    $parameter = $config->get('http_header_parameter');
    if ($config->get('language_detection') === 'http_header') {
      $langCode = $request->headers->get($parameter);
    }
    $data['#cache'] = [
      'max-age' => 600,
      'tags' => ['language_suggestion_http_header'],
      'contexts' => [
        'url.query_args',
        'ip',
      ],
    ];
    $response = new CacheableJsonResponse($langCode);
    $response->addCacheableDependency(CacheableMetadata::createFromRenderArray($data));
    return $response;
  }

  /**
   * Get GEOIP country code from database.
   */
  public function getCountryCode(Request $request) {
    $config = $this->configFactory->get('language_suggestion.settings');
    $data = '';
    $language_detection = $config->get('language_detection');
    if ($language_detection === 'geoip_db') {
      $db_file_id = $config->get('geoip_db_file');
      if ($file = File::load($db_file_id)) {
        try {
          $reader = new Reader($file->getFileUri());
          $record = $reader->country($request->getClientIp());
          $data = !empty($record->country->isoCode) ? $record->country->isoCode : '';
        }
        catch (AddressNotFoundException $e) {
        }
      }
    }
    elseif ($language_detection == '_server') {
      if ($_server_param = $config->get('_server_parameter')) {
        header('cache-control: no-store', FALSE);
        header('Vary: x-geo-country-code', FALSE);
        $data = !empty($_SERVER[$_server_param]) ? $_SERVER[$_server_param] : '';
      }
    }
    if ($config->get('debug')) {
      $this->logger->notice('Detected country:' . $data);
    }
    return new JsonResponse(['country' => $data]);
  }

}
